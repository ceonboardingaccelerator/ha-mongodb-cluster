# ha-mongodb-cluster

User Case
Design, develop and implement a 3 node MongoDB 4.0 cluster (2 data nodes and 1 arbiter node) using IaC. 

Requirements:
CSP of your choosing
Fully managed, do not use DaaS
Cost effective solution
HA within the same region
DR and BackUp
Monitoring
Security Considerations
Documentation: design, maintenance, migration procedure.
MongoDB configuration: No sharding, one replica set, custom db path, authorisation enabled.
More information about MongoDB: https://www.mongodb.com/what-is-mongodb

Extra Features
Once the project has all requirements completed, convert the 3 nodes cluster in an on demand number of nodes cluster depending on DB usage.